package com.jt.user.service;

import com.jt.user.pojo.User;

import java.util.List;

public interface UserService {

    User selectUserByUsername(String username);
    List<String> selectUserPermissions(Long userId);
}
