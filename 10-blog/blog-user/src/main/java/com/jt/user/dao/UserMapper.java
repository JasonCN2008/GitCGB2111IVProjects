package com.jt.user.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jt.user.pojo.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface UserMapper extends BaseMapper<User> {
    /**
     * 基于用户名查询用户信息
     * @param username
     * @return
     */
    @Select("select id,username,password,status " +
            "from tb_users " +
            "where username=#{username}")
    User selectUserByUsername(@Param("username") String username);
    /**
     * 基于用户id查询用户权限
     * @param userId
     * @return
     */
    @Select("select distinct permission " +
            "from tb_menus m join tb_role_menus rm on m.id=rm.menu_id " +
            "     join tb_user_roles ur on rm.role_id=ur.role_id " +
            "where ur.user_id=#{userId}")
    List<String> selectUserPermissions(
            @Param("userId") Long userId);
}
