package com.jt.user.service.impl;
import com.jt.user.dao.UserMapper;
import com.jt.user.pojo.User;
import com.jt.user.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Slf4j
@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserMapper userMapper;

    @Override
    public User selectUserByUsername(String username) {
        log.debug("username is {}",username);
        return userMapper.selectUserByUsername(username);
    }
    @Override
    public List<String> selectUserPermissions(Long userId) {
        log.debug("userId is {}",userId);
        return userMapper.selectUserPermissions(userId);
    }
}
