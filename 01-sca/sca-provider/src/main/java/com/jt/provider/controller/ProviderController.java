package com.jt.provider.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

/**
 * 服务提供方的Controller对象.
 */
@RestController
public class ProviderController {
    /**
     * 基于@Value注解告诉springboot,读取配置文件(bootstrap.yml)中
     * 的server.port的值,假如读取不到则给一个默认值8080.
     */
    @Value("${server.port:8080}")
    private String server;

    //http://ip:port/provider/echo/aaa
    @GetMapping("/provider/echo/{msg}")
    public String doRestEcho1(@PathVariable("msg") String msg){
        //return server+" say hello "+msg;
        //假如不想执行字符串的拼接,可以采用如下方式
        return String.format("%s say hello %s",server,msg);
        //如上语句中的s%为一个字符串占位符,server,msg的值最后会替换s%
    }


}
