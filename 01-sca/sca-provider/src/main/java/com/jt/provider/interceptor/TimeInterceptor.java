package com.jt.provider.interceptor;
import org.springframework.web.servlet.HandlerInterceptor;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.time.LocalTime;

/**
 * 自定义spring mvc中的拦截器对象,基于此对象对请求和响应进行拦截
 */
public class TimeInterceptor implements HandlerInterceptor {
    /**
     * 此方法会在目标controller方法执行之前执行,可以这里对请求进行
     * 预处理,然后在交给后端的controller,同时也可以基于此方法的
     * 返回值,决定请求是继续放行,还是到此结束.
     * 在当前应用中,我们基于访问时间通过拦截器对请求进行限制.
     * @param request
     * @param response
     * @param handler
     * @return  true表示放行,false表示拦截.
     * @throws Exception
     */
    @Override
    public boolean preHandle(HttpServletRequest request,
                             HttpServletResponse response,
                             Object handler)
            throws Exception {
        System.out.println("==preHandle==");
        //1.获取当前时间
        LocalTime now = LocalTime.now();
        //2.获取当前时间的对应的小时
        int hour=now.getHour();
        System.out.println("hour="+hour);
        //3.判断时间是否在允许范围之内
        if(hour<10||hour>=23){
            throw new RuntimeException("请在允许时间访问6~23");
        }
        return true;
    }
}
