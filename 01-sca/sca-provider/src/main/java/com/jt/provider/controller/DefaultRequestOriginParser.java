package com.jt.provider.controller;
import com.alibaba.csp.sentinel.adapter.spring.webmvc.callback.RequestOriginParser;
import org.springframework.stereotype.Component;
import javax.servlet.http.HttpServletRequest;

@Component
public class DefaultRequestOriginParser
        implements RequestOriginParser {
    /**这个方法中基于业务规则对请求数据进行解析,然后进行授权
     * 1)可以对请求参数进行解析
     * 2)可以对请求头数据进行解析
     * 3).......*/
    @Override
    public String parseOrigin(HttpServletRequest request) {
        //对请求参数进行解析，并返回参数值，然后将这个值应用在sentinel的授权规则中
        //http://ip:port/path?origin=xxx
        //String origin=request.getParameter("origin");//这里的名字为参数名
        //return origin;
        //对请求头中的数据进行解析，请求头的名字为token，然后基于值进行授权。
        //String token=request.getHeader("token");
        //return token;
        //对请求ip地址进行解析，然后获取ip值进行授权。
        String ip = request.getRemoteAddr();
        int port=request.getRemotePort();
        System.out.println("ip="+ip);
        return ip;
    }
}
