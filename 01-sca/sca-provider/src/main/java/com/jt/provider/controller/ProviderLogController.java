package com.jt.provider.controller;

import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 通过controller中的方法演示日志级别应用以及配置中心的作用.
 *
 * 假如希望配置中心内容发生变化,要对类中属性进行重新初始化,
 * 可使用此注解@RefreshScope对类进行描述,这个注解描述类时,
 * 一旦配置中心的内容发生了变化,而此类中的有些属性的值又需要
 * 配置中心读取,此时再访问数据时,@RefreshScope注解描述的类
 * 的对象就会重新创建.
 */
@RefreshScope
@Slf4j
@RestController
public class ProviderLogController {

       public ProviderLogController(){
           System.out.println("==ProviderLogController()==");
       }

      //private static final Logger log=
               //LoggerFactory.getLogger(ProviderLogController.class);
       @GetMapping("/provider/log/doLog01")
       public String doLog01(){//日志级别 trace<debug<info<warn<error
           System.out.println("==doLog01==");
           log.trace("==trace==");
           log.debug("==debug==");
           log.info("==info=="); //springboot工程默认日志级别为info
           log.warn("==warn==");
           log.error("==error==");
           return "test log level";
       }

      //定义一个日志级别数据,用于存储从配置文件或配置中心读取的数据
      @Value("${logging.level.com.jt:error}")
      private String logLevel;//属性会在构建对象时初始化

      @GetMapping("/provider/log/doLog02")
      public String doLog02(){
         log.info("log level is  {}",logLevel);
         return  "log level  is "+logLevel;
      }

}
