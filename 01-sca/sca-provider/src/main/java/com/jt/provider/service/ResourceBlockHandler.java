package com.jt.provider.service;

import com.alibaba.csp.sentinel.slots.block.BlockException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * 通过此类处理由@SentinelResource注解描述的方法出现的限流异常.
 */
@Slf4j
@Component
public class ResourceBlockHandler {
    /**
     * 这里的异常处理方法有要求:
     * 1)修饰符 public static
     * 2)返回值类型要与@SentinelResource注解描述方法返回值类型相同
     * 3)参数列表要与@SentinelResource注解描述方法参数列表相同,
     * 可以再最后多添加一个BlockException参数
     * @param ex
     * @return
     */
    public static String doHandle(BlockException ex){
          log.error("被限流了.....,{}",ex);
          return "访问太频繁了";
    }

    public static String doHandle(Integer id,BlockException ex){
        log.error("被限流了.....,{}",ex);
        return "访问太频繁了....";
    }
}
//AbstractSentinelInterceptor/Controller/Service(Proxy)/Aspect/Service(目标)
