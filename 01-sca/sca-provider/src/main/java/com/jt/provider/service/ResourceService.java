package com.jt.provider.service;

import com.alibaba.csp.sentinel.annotation.SentinelResource;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@Service
public class ResourceService {
    /**
     * SentinelResource 注解描述的方法为一个切入点方法,
     * 也就是说对此方法进行访问时,我们要进行一些限流操作.
     * 具体的限流动作会放在切面的通知方法中.
     *
     * 其它:
     * 假如在访问这个资源方法时,出现了限流异常(BlockException),我们
     * 可以自定义异常处理,具体自定的方式,可以通过:
     * 1)blockHandlerClass 指定异常处理类
     * 2)blockHandler指定异常处理方法
     * @return
     */
    @SentinelResource(value="doGetResource",
                      blockHandlerClass = ResourceBlockHandler.class,
                      blockHandler = "doHandle")
    public String doGetResource(){
        return "doGetResource";
    }

    /**
     * SentinelResource 注解描述的方法为一个切入点方法,
     * 也就是说对此方法进行访问时,我们要进行一些限流操作.
     * 具体的限流动作会放在切面的通知方法中.
     *
     * 其它:
     * 假如在访问这个资源方法时,出现了限流异常(BlockException),我们
     * 可以自定义异常处理,具体自定的方式,可以通过:
     * 1)blockHandlerClass 指定异常处理类
     * 2)blockHandler指定异常处理方法
     * @return
     */
    @SentinelResource(value="resource",
            blockHandlerClass = ResourceBlockHandler.class,
            blockHandler = "doHandle")
    public String doGetResource(Integer id){
        //....
        return "the data's id is "+id;
    }
}
