package com.jt;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoUIApplication {//java 包名.类名  100 200
    public static void main(String[] args) {
        SpringApplication.run(DemoUIApplication.class,args);
    }
}
