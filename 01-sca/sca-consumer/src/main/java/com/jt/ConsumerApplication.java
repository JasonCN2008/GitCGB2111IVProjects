package com.jt;

import com.netflix.loadbalancer.IRule;
import com.netflix.loadbalancer.RandomRule;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

/**
 * @EnableFeignClients 用于描述启动类或配置类,
 * 此时项目在启动时,就会启动一个FeignStarter组件,
 * 这个组件会对项目中使用了@FeignClient注解描述
 * 的接口进行代理对象的创建.
 */
@EnableFeignClients
@SpringBootApplication
public class ConsumerApplication {
    public static void main(String[] args) {
        SpringApplication.run(ConsumerApplication.class,args);
    }

    /**
     * 创建RestTemplate对象,我们
     * 可以基于此对象进行远端服务
     * 调用,此对象封装了远程服务调
     * 用的协议和相关方法.
     * @return
     */
    @Bean
    public RestTemplate restTemplate(){
        return new RestTemplate();
    }

    /**
     * 当使用 @LoadBalanced注解描述RestTemplate对象时,
     * 我们使用RestTemplate再次进行服务调用时,系统底层
     * 会对调用过程进行拦截,拦截到请求url后,会基于请求url
     * 中的服务名获取具体的服务实例,然后基于服务实例重新构建新的url,
     * 再基于新的url进行服务调用.
     * @return
     */
    @Bean
    @LoadBalanced
    public RestTemplate loadBalancedRestTemplate(){
        return new RestTemplate();//实例
    }

    /**
     * 假如默认负载均衡策略不能满足我们需求怎么办?
     * 在这里指定一下负载均衡策略?
     * 具体方案:构建IRule接口实现类对象,
     * 并将此对象交给spring管理即可.
     * 这里以随机负载均衡策略为例进行实践.
     */
     @Bean
     public IRule rule(){
         return new RandomRule();
     }

}
