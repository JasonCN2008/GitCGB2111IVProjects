package com.jt.consumer.service;

import feign.hystrix.FallbackFactory;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * @FeignClient 注解描述的接口为[远程服务]调用接口,
 * 此接口中我们应该定义这些元素?
 * 1)你要调用的服务?一般用服务名进行标识
 * 2)访问服务中的什么资源?(数据,资源标识是什么?-url)
 * 3)对服务中的资源进行什么操作?(Get,Post,Put,Delete)
 * 4)资源访问不到怎么办?操作资源的过程中出现异常怎么办? (预案)
 * 此接口是否需要我们自己写实现类?(不需要,由底层创建接口实现类,我们只做声明即可)
 * 此接口对应的对象会交给spring管理吗?(会,这个对象是一个代理对象)
 * 此对象默认的bean名称是什么?(假如没有指定,默认使用name或value属性的值,
 * 推荐基于contextId手动指定一个Bean名字)
 */
@FeignClient(name="sca-provider",
             contextId = "remoteProviderService",
             fallbackFactory = ProviderFallbackFactory.class)
public interface RemoteProviderService {//明修栈道暗度陈仓
      //Rest风格:通过URI定义资源,通过请求动作操作资源
      @GetMapping("/provider/echo/{string}")
      String echoMessage(@PathVariable("string") String msg);
}
