package com.jt.consumer.service;

import feign.hystrix.FallbackFactory;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * Feign回调接口工厂对象,基于此对象创建Feign接口实现类对象.
 * 这个实现类对象,用于做容错处理?(有备无患,这里做熔断处理.)
 */
@Slf4j
@Component
public class ProviderFallbackFactory implements FallbackFactory<RemoteProviderService> {
    //创建日志对象
    //private static final Logger log=
            //LoggerFactory.getLogger(ProviderFallbackFactory.class);
    @Override
    public RemoteProviderService create(Throwable throwable) {
//        return new RemoteProviderService() {//匿名内部类
//            @Override
//            public String echoMessage(String msg) {
//                //...报警...(发短信,email,日志.....)
//                return "服务暂时不可用,稍等片刻再访问";
//            }
//        };
          //如上return语句的简单写法,可采用如下方式:
          return msg -> {//lambda表达式 (JDK8中的语法)
                //...报警...(发短信,email,日志.....)
                log.error("error: {} ","sca-provider服务调用失败");
                return "服务暂时不可用,稍等片刻再访问";
          };
    }
}
