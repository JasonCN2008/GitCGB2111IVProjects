package com.jt.consumer.service;

import org.springframework.cloud.openfeign.FeignClient;

@FeignClient(name="sca-provider",contextId = "otherProviderService")
public interface OtherProviderService {
}
