package com.jt;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.beans.Transient;

@SpringBootTest
public class IntegerTests {

    @Test
    void testInteger(){
       Integer a1=100;
       Integer a2=100;//Integer.valueOf(100)
       Integer a3=300;
       Integer a4=300;
       System.out.println(a1==a2);//true
       System.out.println(a3==a4);//false
    }
}
