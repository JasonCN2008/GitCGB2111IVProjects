package com.jt.net;

import java.io.IOException;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;

public class Tomcat {//Web Server
    public static void main(String[] args) throws IOException {
        //1.构建服务器对象,并在指定端口上进行监听
        //ServerSocket是java中服务端对象
        ServerSocket server=new ServerSocket(9999);
        System.out.println("server start ok");
        //2.不断等待客户端连接
        while(true){
            //等待客户端的连接,这里的socket为客户端对象
            Socket client = server.accept();//这个方法是一个阻塞方法
            new Thread(()->{//1M
                try {
                    //向客户端输出一个hello
                    System.out.println("client=" + client);
                    OutputStream out = client.getOutputStream();
                    //假如客户端是基于http协议发起的请求，这里我们的
                    //响应数据格式应该为http协议格式的。
                    String data = "HTTP/1.1 200 OK \n\r" + //响应行
                            "Content-Type: text/html;charset=UTF-8 \n\r" + //响应头
                            "\n\r" + //空行
                            "hello";//响应体
                    out.write(data.getBytes());
                    out.flush();
                }catch (Exception e){}
            }).start();
        }
    }
}
