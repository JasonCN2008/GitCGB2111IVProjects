package com.jt.filter;

import org.apache.http.HttpHeaders;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.Ordered;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

/**
 * 课堂练习:
 * 构建一个全局的请求url过滤器,
 * 对请求url进行校验，white url则放行，black url则给出反馈。
 */
@ConfigurationProperties("web.request")
@Component
public class BlackUrlGlobalFilter implements GlobalFilter,Ordered {
    //定义black url集合(在此集合中的url不允许访问我们的资源)
    private List<String> blackUrls=new ArrayList<>();
//    public BlackUrlGlobalFilter(){
//        blackUrls.add("/nacos/provider/echo/9002");
//        blackUrls.add("/nacos/provider/echo/9003");
//    }

    public void setBlackUrls(List<String> blackUrls) {
        this.blackUrls = blackUrls;
    }

    /**
     * 这是一个对请求进行过滤的方法
     * @param exchange 基于这个对象可以获取请求和响应对象
     * @param chain 这里表示过滤链
     * @return 这里的返回值为一个Mono对象(本质上是一个Publisher)
     * ,这是spring5.0以后，WebFlux框架中的一个对象。
     */
    @Override
    public Mono<Void> filter(ServerWebExchange exchange,
                             GatewayFilterChain chain) {
        //1.获取请求和响应对象
        ServerHttpRequest request = exchange.getRequest();
        ServerHttpResponse response = exchange.getResponse();
        //2.获取请求路径(不需要ip和端口),例如/nacos/provider/echo/9001
        String requestPath=request.getURI().getPath();
        System.out.println("requestPath="+requestPath);
        //3.判定请求路径是否是不允许的url，假如是则给出反馈
        if(blackUrls.contains(requestPath)){
            //结果处理方案1：抛出异常
           //throw new RuntimeException("这个请求已被列入black url");
            //结果处理方案2：响应一个error消息(难度系数要高一些)
            //告诉浏览器响应的数据类型以及以什么编码呈现数据
            response.getHeaders()
                 .add(HttpHeaders.CONTENT_TYPE,
                            "text/html;charset=utf-8");
            //封装响应数据
            DataBuffer data=response.bufferFactory()
                 .wrap("当前url不允许访问资源".getBytes(StandardCharsets.UTF_8));
            return response.writeWith(Mono.just(data));
        }
        //继续执行下一步操作
        return chain.filter(exchange);
    }
    /**定义过滤器的优先级，数字越小，优先级越高*/
    @Override
    public int getOrder() {
        return -1;
    }
}
