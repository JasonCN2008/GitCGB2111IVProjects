package com.jt;

import com.alibaba.csp.sentinel.adapter.gateway.sc.callback.BlockRequestHandler;
import com.alibaba.csp.sentinel.adapter.gateway.sc.callback.GatewayCallbackManager;
import com.alibaba.csp.sentinel.adapter.spring.webmvc.callback.BlockExceptionHandler;
import com.alibaba.fastjson.JSON;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.server.ServerResponse;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import java.util.HashMap;
import java.util.Map;

//@Configuration
public class GatewayConfig {
    public GatewayConfig(){//了解
        GatewayCallbackManager.setBlockHandler(
                new BlockRequestHandler() {
            @Override
            public Mono<ServerResponse> handleRequest(
                    ServerWebExchange serverWebExchange,
                    Throwable throwable) {
                Map<String,Object> map=new HashMap<>();
                map.put("state",429);
                map.put("message","two many request");
                String jsonStr= JSON.toJSONString(map);
                //构建Mono对象
                return ServerResponse.ok()//获取一个Builder对象
                        //构建Mono对象(Publisher)
                        .body(Mono.just(jsonStr),//封装响应内容
                                String.class);
            }
        });
    }
}
