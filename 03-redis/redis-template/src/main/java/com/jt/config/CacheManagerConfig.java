package com.jt.config;
import org.springframework.cache.CacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.cache.RedisCacheConfiguration;
import org.springframework.data.redis.cache.RedisCacheManager;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.serializer.RedisSerializationContext;
import org.springframework.data.redis.serializer.RedisSerializer;

/**
 * 定义缓存配置类,在此配置类中
 * 修改默认的缓存配置
 */
@Configuration
public class CacheManagerConfig {
    /**
     * 定制缓存管理器对象.
     * @param connectionFactory
     * @return
     */
    @Bean
    public CacheManager cacheManager(RedisConnectionFactory connectionFactory){
       RedisCacheConfiguration redisCacheConfiguration=
               RedisCacheConfiguration.defaultCacheConfig()
                //指定key序列化方式
               .serializeKeysWith(RedisSerializationContext
                       .SerializationPair
                       .fromSerializer(RedisSerializer.string()))
                //指定value的序列化方式
               .serializeValuesWith(RedisSerializationContext
                       .SerializationPair
                       .fromSerializer(RedisSerializer.json()));

       return RedisCacheManager.builder(connectionFactory)
               .cacheDefaults(redisCacheConfiguration).build();
    }
}
