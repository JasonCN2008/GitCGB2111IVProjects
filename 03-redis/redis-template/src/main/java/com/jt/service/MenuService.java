package com.jt.service;
import com.jt.pojo.Menu;
public interface MenuService {
    /**
     * 基于id查询菜单对象
     * @param id
     * @return
     */
    Menu selectById(Long id);
    /**
     * 创建一个新的菜单对象
     * @param menu
     * @return
     */
    Menu insertMenu(Menu menu);
    /**
     * 更新菜单对象
     * @param menu
     * @return
     */
    Menu updateMenu(Menu menu);
}
