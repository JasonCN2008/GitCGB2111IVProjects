package com.jt.service.impl;

import com.jt.dao.MenuMapper;
import com.jt.pojo.Menu;
import com.jt.service.MenuService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

@Service
public class AopMenuServiceImpl implements MenuService {

    @Autowired
    private MenuMapper menuMapper;
    private static final Logger log= LoggerFactory.getLogger(AopMenuServiceImpl.class);

    /**
     * @Cacheable 注解描述的方法为缓存切入点方法,
     * 表示在执行这个方法时,先从缓存去取数据,缓存有则直接返回,
     * 缓存没有则执行数据库的查询操作
     *
     * 1)value属性的值一般会作为key前缀(建议写模块名)
     * 2)key属性的值一般会作为key的后缀(能够具备唯一性)
     * @param id
     * @return
     */
    @Cacheable(value="menuCache",key = "#id")//menuCache::1
    @Override
    public Menu selectById(Long id) {
        log.info("Get Data from MySql");
        return menuMapper.selectById(id);
    }
    /**
     * @CachePut 注解描述的方法为缓存切入点方法,
     * 当执行完它描述的方法后,会将方法的返回值存储到cache
     * ,key由@CachePut注解指定,值为方法的返回值.
     * @param menu
     * @return
     */
    @CachePut(value="menuCache",key = "#menu.id")
    @Override
    public Menu insertMenu(Menu menu) {
        menuMapper.insert(menu);
        return menu;
    }


    @CachePut(value="menuCache",key = "#menu.id")
    @Override
    public Menu updateMenu(Menu menu) {
        menuMapper.updateById(menu);
        return menu;
    }
}
