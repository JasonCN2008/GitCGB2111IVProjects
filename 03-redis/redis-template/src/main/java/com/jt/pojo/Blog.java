package com.jt.pojo;

import java.io.Serializable;

public  class Blog implements Serializable {
    private static final long serialVersionUID = 8397600816070129636L;
    private Long id;
    private String title;
    private String content;
    private String remark;

    public void setRemark(String remark) {
        this.remark = remark;
    }

    private String getRemark() {
        return remark;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getContent() {
        return content;
    }

    @Override
    public String toString() {
        return "Blog{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", content='" + content + '\'' +
                ", remark='" + remark + '\'' +
                '}';
    }
}
