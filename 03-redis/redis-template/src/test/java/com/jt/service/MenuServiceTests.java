package com.jt.service;

import com.jt.pojo.Menu;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class MenuServiceTests {

    //@Autowired
    //private MenuService menuService;

    @Autowired
    @Qualifier(value="aopMenuServiceImpl")
    private MenuService menuService;

    @Test
    void testSelectById(){
        Menu menu=menuService.selectById(5L);
        System.out.println(menu);
    }
    @Test
    void testInsertMenu(){
        Menu menu=new Menu();
        menu.setName("export resource");
        menu.setPermission("sys:res:export");
        menuService.insertMenu(menu);
    }

    @Test
    void testUpdateMenu(){
        Menu menu = menuService.selectById(4L);
        menu.setName("dao ru resource");
        menuService.updateMenu(menu);
    }
}
