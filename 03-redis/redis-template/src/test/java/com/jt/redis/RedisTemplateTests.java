package com.jt.redis;
import com.jt.pojo.Blog;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.DataAccessException;
import org.springframework.data.redis.connection.RedisConnection;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisCallback;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.data.redis.serializer.RedisSerializer;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.time.Duration;
import java.util.Arrays;
import java.util.Locale;
import java.util.Map;

@SpringBootTest
public class RedisTemplateTests {
    /**
     * RedisTemplate 对象在存取数据时默认会按照如下方式执行:
     * 1)key和value在存储时,会基于JDK方式进行序列化
     * 2)基于key取值时,默认会基于JDK做反序列化.
     */
    @Autowired
    private RedisTemplate redisTemplate;

    /**
     * 应用redisTemplate对象的另一种方式
     */
    @Test
    void testFlushdb(){
        redisTemplate.execute(new RedisCallback() {
            @Override
            public Object doInRedis(
                    RedisConnection redisConnection)
                    throws DataAccessException {
                //这里的RedisConnection表示连接
                //通过这里的连接对象,你可以执行一些其它数据库操作
                redisConnection.flushAll();
                return "ok";
            }
        });
    }

    /**
     * 课堂练习 15:00
     */
    @Test
    void testHashOper02() throws InvocationTargetException, IllegalAccessException {
        //1.创建Blog对象
        Blog blog1=new Blog();
        blog1.setId(100L);
        blog1.setTitle("Title-A");
        blog1.setContent("Content-AAA");
        //2.将Blog对象写入到redis
        //2.1序列化存取方案1:
        ValueOperations valueOperations =
                redisTemplate.opsForValue();
        valueOperations.set("blog:100", blog1,
                Duration.ofSeconds(60));
        Blog blog2=(Blog)valueOperations.get("blog:100");
        System.out.println(blog2);
        //2.2序列化存取方案2:
        HashOperations hashOperations =
                redisTemplate.opsForHash();
        //直接存储
        //hashOperations.put("blog:200", "id", blog1.getId());
        //hashOperations.put("blog:200", "title", blog1.getTitle());
        //hashOperations.put("blog:200", "content", blog1.getContent());

        //Map map=hashOperations.entries("blog:200");
        //System.out.println(map);
        //反射存储(序列化)
        Class<?> clazz=blog1.getClass();//字节码对象(反射起点)
        Method[] methods=clazz.getDeclaredMethods();//获取类中所有方法
        for(Method m:methods){
           if(m.getName().startsWith("get")){//getId,getTitle
               String temp=m.getName().substring(3);//去掉get单词,Id,Title,
               hashOperations.put("blog:200",
                       temp.substring(0,1).toLowerCase()//去掉get后的第一个字母小写
                               +temp.substring(1),
                       m.invoke(blog1));// m.invoke(blog1)表示执行blog1的get方法
           }
        }
        Map entries = hashOperations.entries("blog:200");
        System.out.println(entries);

    }

    @Test
    void testHashOper01(){
        //1.获取Hash类型操作对象
        //也可以自己定义序列化方式
        //redisTemplate.setKeySerializer(RedisSerializer.string());
        //redisTemplate.setHashKeySerializer(RedisSerializer.string());
        //redisTemplate.setHashValueSerializer(RedisSerializer.string());
        HashOperations hashOperations =
                redisTemplate.opsForHash();
        //2.读写数据
        hashOperations.put("blog:1","id","1");
        hashOperations.put("blog:1", "title", "hello redis");
        Map entries = hashOperations.entries("blog:1");
        System.out.println(entries);
    }

    @Test
    void testStringOper02(){
        //1.修改key/value的序列化方式
        redisTemplate.setKeySerializer(RedisSerializer.string());
        redisTemplate.setValueSerializer(RedisSerializer.string());
        //2.获取redis操作对象
        ValueOperations valueOperations =
                redisTemplate.opsForValue();
        valueOperations.set("code:11",
                "ABCD",Duration.ofSeconds(60));
        Object o = valueOperations.get("code:11");
        System.out.println(o);
    }

    @Test
    void testStringOper01(){
        //1.获取redis操作对象
        ValueOperations valueOperations =
                redisTemplate.opsForValue();
        //2.读写数据
        valueOperations.set("x", 100);//采用jdk默认序列化规则对key/value进行序列化
        //valueOperations.increment("x");不可以
        Object x = valueOperations.get("x");
        System.out.println(x);
        valueOperations.increment("y");//第一次执行这个操作时,要确保redis中没有y
        valueOperations.increment("y");
        Long y = valueOperations.increment("y");

        System.out.println(y);//3,6,9,....

        //存储数据时,给定一个有效期.
        valueOperations.set("z",
                200, Duration.ofSeconds(10));
    }

    @Test
    void testGetConnection(){
        RedisConnection connection =
                redisTemplate.getConnectionFactory()
                        .getConnection();
        String result = connection.ping();
        System.out.println(result);
    }
}
