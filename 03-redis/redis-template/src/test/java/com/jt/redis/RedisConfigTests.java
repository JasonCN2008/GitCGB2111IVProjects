package com.jt.redis;

import com.jt.pojo.Blog;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;

@SpringBootTest
public class RedisConfigTests {

    @Autowired
    private RedisTemplate simpleRedisTemplate;

    @Autowired
    private RedisTemplate redisTemplate;

    @Test
    void testBlogOper2(){
        Blog blog1=new Blog();
        blog1.setId(100L);
        blog1.setTitle("Title-AA");
        blog1.setContent("Content-AAA");
        blog1.setRemark("Remark-AAA");
        ValueOperations valueOperations =
                redisTemplate.opsForValue();
        valueOperations.set("blog:100", blog1);
        Blog blog2=(Blog)valueOperations.get("blog:100");
        System.out.println(blog2);
    }
    @Test
    void testBlogOper1(){
        Blog blog1=new Blog();
        blog1.setId(100L);
        blog1.setTitle("Title-AA");
        blog1.setContent("Content-AAA");
        //blog1.setRemark("Remark-AAA");
        ValueOperations valueOperations =
                simpleRedisTemplate.opsForValue();
        valueOperations.set("blog:100", blog1);
        Blog blog2=(Blog)valueOperations.get("blog:100");
        System.out.println(blog2);
    }
}
