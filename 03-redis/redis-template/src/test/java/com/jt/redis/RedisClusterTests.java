package com.jt.redis;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.connection.RedisConnection;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;

@SpringBootTest
public class RedisClusterTests {

    @Autowired
    private RedisTemplate redisTemplate;
    @Test
    void testGetConnection(){
        RedisConnection connection =
                redisTemplate.getConnectionFactory()
                        .getConnection();
        System.out.println(connection);
    }
    @Test
    void testCluster(){
        ValueOperations valueOperations =
                redisTemplate.opsForValue();
        valueOperations.set("code", "ABCD");
        Object code = valueOperations.get("code");
        System.out.println(code);
    }
}
