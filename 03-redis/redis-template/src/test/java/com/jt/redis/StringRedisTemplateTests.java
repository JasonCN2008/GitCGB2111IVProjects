package com.jt.redis;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jt.pojo.Blog;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.ValueOperations;

@SpringBootTest
public class StringRedisTemplateTests {
    /**
     * StringRedisTemplate 继承RedisTemplate对象,
     * 此对象默认采用string类型进行序列化方式的数据操作
     */
      @Autowired
      private StringRedisTemplate stringRedisTemplate;
      @Test
      void testStringOper() throws JsonProcessingException {
          ValueOperations<String, String> valueOperations =
                  stringRedisTemplate.opsForValue();
          valueOperations.set("x", "100");
          valueOperations.increment("x");
          String x = valueOperations.get("x");
          System.out.println(x);
          //存储一个对象
          Blog blog1=new Blog();
          blog1.setId(100L);
          blog1.setTitle("Title-AA");
          blog1.setContent("Content-AAA");
          valueOperations.set("blog:100",
                  //将对象转换为json格式字符串进行存储
                  new ObjectMapper().writeValueAsString(blog1));
          //读取对象内容
          String blogJsonStr=valueOperations.get("blog:100");
          System.out.println(blogJsonStr);
          //将json字符串转换为Blog对象(ObjectMapper为jack中的api)
          Blog blog2 =
                  new ObjectMapper().readValue(blogJsonStr,
                  Blog.class);
          System.out.println(blog2);
      }
}
