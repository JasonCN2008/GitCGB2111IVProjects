package com.jt;

import org.junit.Test;
import redis.clients.jedis.HostAndPort;
import redis.clients.jedis.JedisCluster;

import java.util.HashSet;
import java.util.Set;

public class JedisClusterTests {


    @Test
    public void testJedisCluster(){
        String host="192.168.126.128";
        Set<HostAndPort> nodes=new HashSet<>();
        nodes.add(new HostAndPort(host, 8010));
        nodes.add(new HostAndPort(host, 8011));
        nodes.add(new HostAndPort(host, 8012));
        nodes.add(new HostAndPort(host, 8013));
        nodes.add(new HostAndPort(host, 8014));
        nodes.add(new HostAndPort(host, 8015));
        JedisCluster jedisCluster= new JedisCluster(nodes);
        jedisCluster.set("class", "cgb2111");
        String aClass = jedisCluster.get("class");
        System.out.println(aClass);
        jedisCluster.close();

    }
}
