package com.jt;

import com.google.gson.Gson;
import org.junit.Test;
import redis.clients.jedis.Jedis;

import java.util.HashMap;
import java.util.Map;

public class JedisTests {
    /**
     * 测试hash类型数据
     */
    @Test
    public void testHashOper01(){
        //1.建立连接(TCP)
        Jedis jedis=new Jedis(
                "192.168.126.128", 6379);
        //2.添加数据
        Map<String,String> map1=new HashMap<>();
        map1.put("id", "201");
        map1.put("title", "redis6.2.5");
        jedis.hset("blog:201", map1);//不存在则直接添加
        jedis.hset("blog:201", "title", "redis6.2.6");
        //3.读取数据
        map1=jedis.hgetAll("blog:201");
        System.out.println(map1);
        //4.释放资源
        jedis.close();
    }
    /**
     * 测试字符串操作
     */
    @Test
    public void testStringOper02() {
        //1.建立连接
        Jedis jedis=new Jedis(
                "192.168.126.128", 6379);
        //2.将一个map对象转换为json字符串,然后写入到redis并给定有效期
        Map<String,String> map1=new HashMap<>();
        map1.put("id", "101");
        map1.put("title", "redis6.2.5");
        Gson gson=new Gson();
        String jsonStr1=gson.toJson(map1);
        System.out.println(jsonStr1);
        jedis.set("blog:101", jsonStr1);
        jedis.expire("blog:101", 20);
        //3.从redis将json字符串读出,并转换为map然后输出.
        String jsonStr2 = jedis.get("blog:101");
        Map<String,String> map2=gson.fromJson(jsonStr2,Map.class);
        System.out.println(map2);
        //4.释放资源
        jedis.close();
    }
    /**
     * 测试字符串操作
     */
    @Test
    public void testStringOper01(){
      //1.建立连接
        Jedis jedis=new Jedis(
                "192.168.126.128", 6379);
      //2.添加数据(Create)
        jedis.set("id", "100");
        Long setnx = jedis.setnx("lock", "AAA");
        jedis.set("logs", "abc");
        //3.获取数据(Retrieve)
        String id = jedis.get("id");
        System.out.println("update.before.id="+id);
      //4.修改数据(Update)
        jedis.incr("id");
        id = jedis.get("id");
        System.out.println("update.after.id="+id);

        jedis.expire("lock", 10);
        jedis.append("logs","EF");

      //5.删除数据(Delete)
        jedis.del("id");
        id = jedis.get("id");
        System.out.println("delete.after.id="+id);
      //6.释放资源
        jedis.close();

    }

    @Test
    public void testGetConnection(){
        //1.建立连接
        //假如无法建立连接,要从如下几个方面进行分析
        //1)ip
        //2)port
        //3)防火墙
        //4)redis服务是否启动ok
        Jedis jedis=new Jedis(
                "192.168.126.128", 6379);
        //jedis.auth("123456");
        //2.执行ping操作
        String result = jedis.ping();
        System.out.println(result);
    }
}
