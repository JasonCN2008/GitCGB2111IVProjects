package com.jt;

import org.junit.Test;
import redis.clients.jedis.Jedis;

public class JedisMasterSlaveTests {

    @Test
    public void testMasterReadAndWrite(){
        Jedis jedis=new Jedis("192.168.126.128", 6379);
        jedis.set("id", "100");
        String id = jedis.get("id");
        System.out.println("id="+id);
        jedis.close();
    }
    @Test
    public void testSlaveRead(){
        Jedis jedis=new Jedis("192.168.126.128", 6380);
        //jedis.incr("id"); //不允许执行任何的write操作
        String id = jedis.get("id");
        System.out.println("id="+id);
        jedis.close();
    }
}
