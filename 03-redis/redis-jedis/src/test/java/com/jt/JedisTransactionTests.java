package com.jt;

import com.jt.datasource.JedisDataSource;
import org.junit.Test;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.Transaction;

import java.util.List;

public class JedisTransactionTests {

    /**
     * 模拟一个转账操作
     * 1)初始数据 tony 1000，jack 200
     * 2)实现tony向jack转账，转100元
     */
    @Test
    public void testTransfer(){
        Jedis jedis = JedisDataSource.getConnection();
        jedis.set("tony", "1000");
        jedis.set("jack", "200");
        //开启事务
        Transaction tx = jedis.multi();
        try {
            tx.decrBy("tony", 100);
            tx.incrBy("jack", 100);
            //int a=100,b=0;
            //a=a/b;
            List<Object> exec = tx.exec();
            System.out.println(exec);
        }catch (Exception e){
            tx.discard();
            throw e;
        }finally {
            jedis.close();
        }

    }
}
