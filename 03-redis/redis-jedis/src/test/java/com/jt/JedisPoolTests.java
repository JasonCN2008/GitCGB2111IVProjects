package com.jt;

import org.apache.commons.pool2.impl.GenericObjectPoolConfig;
import org.junit.Test;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

public class JedisPoolTests {
    /**
     * Jedis连接池应用测试
     * 注意:所有连接池都有享元模式的应用,以实现对象重用.
     */
    @Test
    public void testJedisPool(){
        //1.创建池对象
        //GenericObjectPoolConfig poolConfig=
                //new GenericObjectPoolConfig();
        JedisPoolConfig poolConfig=//这个类型是上面类型的子类类型.
                new JedisPoolConfig();
        poolConfig.setMaxTotal(128);//最大连接数
        poolConfig.setMaxIdle(8);//最大空闲连接数
        JedisPool jedisPool=
                //new JedisPool("192.168.126.128", 6379);
                new JedisPool(poolConfig,
                        "192.168.126.128", 6379);
        //2.获取连接
        Jedis resource = jedisPool.getResource();
        //3.读写数据
        resource.set("id", "100");
        String id = resource.get("id");
        System.out.println(id);
        //4.释放资源
        resource.close();
        jedisPool.close();//以后这个池是关服务的时候关.
    }
}
