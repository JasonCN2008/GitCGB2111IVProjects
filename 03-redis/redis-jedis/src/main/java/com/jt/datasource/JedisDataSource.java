package com.jt.datasource;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

public class JedisDataSource {

    //1.方案1:初始化池对象并获取连接
//    private static JedisPool jedisPool;
//    static{
//        JedisPoolConfig poolConfig=
//                new JedisPoolConfig();
//        poolConfig.setMaxTotal(128);//最大连接数
//        poolConfig.setMaxIdle(8);//最大空闲连接数
//        jedisPool=
//                new JedisPool(poolConfig,
//                        "192.168.126.128", 6379);
//    }
    /**
     * 获取连接
     * @return
     */
//    public static Jedis getConnection(){
//        return jedisPool.getResource();
//    }

   //2.方案2:初始化池对象并获取连接 (何时需要何时创建池对象~懒加载)
    private static volatile JedisPool jedisPool;
    /**
     * volatile 关键字特点:
     * 1)修饰属性
     * 2)禁止指令重排序(JVM底层执行指令时,可能考虑到一定优化,会对指令进行重排序)
     * 3)保证线程可见性(一个线程修改了这个值,其它线程立刻可见.)
     */
    public static  Jedis getConnection(){ //JedisDataSource.class
        if(jedisPool==null) {
            synchronized (JedisDataSource.class) {
                if (jedisPool == null) {
                    JedisPoolConfig poolConfig = new JedisPoolConfig();
                    poolConfig.setMaxTotal(128);//最大连接数
                    poolConfig.setMaxIdle(8);//最大空闲连接数
                    jedisPool = new JedisPool(poolConfig, "192.168.126.128", 6379);
                    //1.对象创建的过程?
                    //1)分配内存
                    //2)初始化属性
                    //3)执行构造方法
                    //4)将JedisPool对象赋值给jedisPool变量
                }
            }
        }
        return jedisPool.getResource();
    }

    public static void close(){
        jedisPool.close();
    }
}
