package com.jt.project;

import com.jt.datasource.JedisDataSource;
import redis.clients.jedis.Jedis;

import java.util.Map;

/**
 * 简易购物车实现
 */
public class CartDemo01 {
    /**
     * 向购物车添加商品
     * @param userId
     * @param goods1
     * @param num
     */
    static void addCart(Long userId,Long goods1,int num){
        Jedis jedis= JedisDataSource.getConnection();
        jedis.hincrBy("cart:"+userId,
                       String.valueOf(goods1),num);
        jedis.close();
    }

    /**
     * 查询用户购物车商品
     * @param userId
     * @return
     */
    static Map<String, String> listCart(Long userId){
        Jedis jedis= JedisDataSource.getConnection();
        Map<String, String> userCart =
                jedis.hgetAll("cart:" + userId);
        jedis.close();
        return userCart;
    }
    public static void main(String[] args) {
       //1.定义用户
        Long userId=1001L;
       //2.定义商品id
        Long goods1=201L;
        Long goods2=202L;
        Long goods3=203L;
       //3.将商品添加到购物车
        addCart(userId,goods1,1);
        addCart(userId,goods2,2);
        addCart(userId,goods3,3);
       //4.显示购车商品.
        Map map=listCart(userId);
        System.out.println(map);
    }
}
