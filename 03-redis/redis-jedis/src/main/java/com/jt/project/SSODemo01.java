package com.jt.project;

import com.jt.datasource.JedisDataSource;
import redis.clients.jedis.Jedis;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.UUID;
/**
 * 需求分析: 单点登录系统
 * 方案设计: 将用户登录状态存储到redis
 * 1)完成用户身份的认证(将登录状态存储到redis,并返回客户端一个token)
 * 2)完成资源访问的授权(检查是否已认证,已认证则授权访问)
 * SSO单点登录实现?
 * 1)定义用户信息
 * 2)定义登录认证方法
 * 3)定义资源访问方法
 */
public class SSODemo01 {
    static String doLogin(String username,String password){
        //1.参数校验
        if(username==null||"".equals(username))
            throw new IllegalArgumentException("username can not be null");
        //2.基于用户名查找用户信息(将来这里要查询数据库)
        if(!"tony".equals(username))
            throw new RuntimeException("用户不存在");
        //3.判定密码是否正确(这里的密码将来要加密后比对)
        if(!"123456".equals(password))
            throw new RuntimeException("密码不正确");
        //4.将用户状态存储到redis
        Jedis jedis=JedisDataSource.getConnection();
        String token= UUID.randomUUID().toString();
        jedis.hset(token, "username", username);
        jedis.hset(token, "permissions", "sys:res:list,sys:res:create");
        jedis.expire(token, 60);//登录时效
        jedis.close();
        //5.返回一个令牌(token)
        return token;
    }

    /**
     * 资源访问
     * @param token
     * @return
     */
    static Object doGetResource(String token){
      //1.参数校验
        if(token==null||"".equals(token))
            throw new IllegalArgumentException("please login");
      //2.基于token查询redis数据
        Jedis jedis=JedisDataSource.getConnection();
        Map<String, String> userMap = jedis.hgetAll(token);
        jedis.close();
      //3.判定用户是否已认证  
        if(userMap==null||userMap.size()==0)
          throw new RuntimeException("please login");
      //4.判定用户是否有这个资源的访问权限
        String permissions = userMap.get("permissions");
        String[] permissionArray = permissions.split(",");
        List<String> permissionList
                = Arrays.asList(permissionArray);
        if(!permissionList.contains("sys:res:list"))
            throw new RuntimeException("No Permission");
        //5.有权限则返回你要访问的资源
        return "your resource";
    }

    public static void main(String[] args) throws InterruptedException {
      // 1)定义用户信息
        String username="tony";
        String password="123456";
      // 2)执行登录认证
        String token=doLogin(username,password);
        System.out.println(token);
      // 3)执行资源访问
        Object resource=doGetResource(token);
        System.out.println(resource);
    }
}
