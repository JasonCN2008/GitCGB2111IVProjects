package com.jt.project;

import java.util.Arrays;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.function.Consumer;

public class LambdaTests {
    public static void main(String[] args) {
        List<String> list = Arrays.asList("A", "B", "C");
        //迭代集合中元素
        //list.forEach(s-> System.out.println(s));//lambda
        //list.forEach(System.out::println);//方法引用

        String strArray[]={"abc","ab","a"};
        Arrays.sort(strArray, (o1,o2)->{
                return o1.length()-o2.length();
        });

        Arrays.asList(strArray)
                .forEach(System.out::println);



    }
}
