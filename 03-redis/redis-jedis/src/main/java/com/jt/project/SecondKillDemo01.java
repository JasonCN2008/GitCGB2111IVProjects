package com.jt.project;

import com.jt.datasource.JedisDataSource;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.Transaction;

import java.util.List;

public class SecondKillDemo01 {
    static void secKill(){
        Jedis jedis= JedisDataSource.getConnection();
        jedis.watch("ticket","money");
        Transaction tx = jedis.multi();
        try {
            tx.decr("ticket");
            tx.incrBy("money", 100);
            List<Object> exec = tx.exec();
            System.out.println(exec);
        }catch (Exception e){
            tx.discard();
            throw  e;
        }finally{
            jedis.unwatch();
            jedis.close();
        }
    }
    public static void main(String[] args) {
        Jedis jedis= JedisDataSource.getConnection();
        jedis.set("ticket", "1");
        jedis.set("money", "0");
        jedis.close();
        Thread t1=new Thread(new Runnable() {
            @Override
            public void run() {
                secKill();
            }
        });
        Thread t2=new Thread(()-> {//lambda
                secKill();
        });
        t1.start();
        t2.start();
    }
}
