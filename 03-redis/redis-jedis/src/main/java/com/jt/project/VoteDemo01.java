package com.jt.project;

import com.jt.datasource.JedisDataSource;
import redis.clients.jedis.Jedis;

import java.util.Set;

/**
 * 基于某个活动,设计一个简易的投票系统:
 * 1)活动(唯一标识是活动id)
 * 2)用户基于活动进行投票
 * 2.1)同一个用户只能投票1次(不允许匿名投票)
 * 2.2)假如已经参与过投票,再投票就是取消投票.
 * 2.3)可以查看总票数,谁参(用户id)与过这个活动的投票.
 */
public class VoteDemo01 {
    /**
     * 执行投票操作
     * @param activityId 活动id,被投票的对象
     * @param userId 投票用户
     * @return 是否投票成功
     */
    static boolean doVote(String activityId,String userId){
        //1.参数校验(自己玩)
        //2.执行投票
        //2.1建立与redis的连接
        Jedis jedis= JedisDataSource.getConnection();
        //2.2编写投票逻辑(基于set集合类型存储投票信息)
        Boolean flag = jedis.sismember(activityId, userId);
        if(flag){//true标识已经投过票,此时要取消已投过的票
            jedis.srem(activityId, userId);
            flag=false;
        }else{//没有投票过票,则执行投票
            jedis.sadd(activityId, userId);
            flag=true;
        }
        //2.3释放资源
        jedis.close();
        //3.返回投票结果
        return flag;
    }

    /**
     * 获取活动的总票数
     * @param activityId
     * @return
     */
    static Long doCount(String activityId){
        Jedis jedis=JedisDataSource.getConnection();
        Long count = jedis.scard(activityId);
        jedis.close();
        return count;
    }

    /**
     * 获取哪些人参与了投票
     * @param activityId
     * @return
     */
    static Set<String> doGetUsers(String activityId){
        Jedis jedis=JedisDataSource.getConnection();
        Set<String> members = jedis.smembers(activityId);
        jedis.close();
        return members;
    }

    public static void main(String[] args) {
        //1.定义活动(集体春游活动)~用户活动id做标识
        String activityId="101";
        //2.定义参与活动的用户
        String user1="2001";
        String user2="2002";
        String user3="2003";
        //3.执行用户投票操作
        boolean flag1=doVote(activityId,user1);
        boolean flag2=doVote(activityId,user2);
        boolean flag3=doVote(activityId,user3);
        boolean flag4=doVote(activityId,user1);
        //4.查看投票总数
        long count=doCount(activityId);
        System.out.println(activityId+"的总投票数:"+count);
        //5.查看哪些用户参与了投票.
        Set<String> userIds=doGetUsers(activityId);
        System.out.println("参与这个活动的用户为:"+userIds);
    }
}
