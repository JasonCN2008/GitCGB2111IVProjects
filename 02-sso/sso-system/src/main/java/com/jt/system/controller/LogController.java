package com.jt.system.controller;

import com.jt.system.pojo.Log;
import com.jt.system.service.LogService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RequestMapping("/log")
@RestController
public class LogController {
    @Autowired
    private LogService logService;

    @PostMapping
    public void doInsertLog(@RequestBody Log userLog){
        //输出系统调试日志
        log.debug("LogController's insert.thread name is {}",
                Thread.currentThread().getName());
        logService.insertLog(userLog);
    }

}
