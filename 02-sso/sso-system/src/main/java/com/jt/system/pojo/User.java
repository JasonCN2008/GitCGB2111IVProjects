package com.jt.system.pojo;
import lombok.Data;
import java.io.Serializable;

/**
 * 基于此对象封装用户信息
 * 例如:登录时,我们可以基于用户从数据库查询用户信息,
 * 并将用户信息存储到此对象.
 *
 * 建议:
 * 1)用于封装值的对象都添加set/get/toString等方法
 * 2)用于封装值的对象都实现序列化接口,并手动添加序列化ID.
 *
 */
@Data
public class User implements Serializable {
    private static final long serialVersionUID = 4831304712151465443L;
    private Long id;
    private String username;
    private String password;
    private String status;
}
