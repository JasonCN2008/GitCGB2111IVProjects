package com.jt.system.service.impl;

import com.jt.system.dao.LogMapper;
import com.jt.system.pojo.Log;
import com.jt.system.service.LogService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class LogServiceImpl implements LogService {

    @Autowired
    private LogMapper logMapper;

    /**
     *  @Async 描述的方法为一个异步切入点方法,
     *  此方法在执行时,可以从spring自带的线程池
     *  中获取一个线程来执行这个方法,而不是使用
     *  web服务池中的线程.这样web服务中的线程
     *  就可以去服务于新的客户端请求服务了。
     *  但是这个异步任务要启动，需要在启动类
     *  或配置上添加@EnableAsync注解
     */
    @Async
    @Override
    public void insertLog(Log userLog) {
        //输出系统调试日志
        log.debug("LogService's insert.thread name is {}",
                Thread.currentThread().getName());
        //try{Thread.sleep(5000);}catch (Exception e){}
        logMapper.insert(userLog);
    }
}
