#1. 基于用户名查询用户信息
select id,username,password,status from tb_users where username='admin';

#2. 基于用户id查询用户权限(这个权限在菜单表中tb_menus)

#2.1 方案1(单表多次查询)
#2.1.1 基于用户id查询用户角色
select role_id from tb_user_roles where user_id=1;
#2.1.2 基于用户角色id查询菜单id
select menu_id from tb_role_menus where role_id in (1);
#2.1.3 基于菜单id查询菜单的权限标识
select permission from tb_menus where id in (1,2,3);

#2.2 方案2(多表嵌套查询)
select permission
from tb_menus
where id in (select menu_id
             from tb_role_menus
             where role_id in (select role_id
                               from tb_user_roles
                               where user_id=1));

#2.3 方案3(多表关联查询)
select distinct permission
from tb_menus m join tb_role_menus rm on m.id=rm.menu_id
                join tb_user_roles ur on rm.role_id=ur.role_id
where ur.user_id=1