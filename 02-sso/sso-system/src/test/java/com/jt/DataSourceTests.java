package com.jt;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

@SpringBootTest
public class DataSourceTests {
    /**
     * 这个数据源对象为JDBC中,从连接池获取连接的一个对象.
     */
    @Autowired
    private DataSource dataSource;//默认HikariDataSource

    /**
     * 测试是否可以获取一个数据库连接
     * @throws SQLException
     */
    @Test
    void testGetConnection() throws SQLException {
        Connection connection =
                dataSource.getConnection();
        System.out.println(connection);
    }
}
