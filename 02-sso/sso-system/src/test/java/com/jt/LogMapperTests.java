package com.jt;

import com.jt.system.dao.LogMapper;
import com.jt.system.pojo.Log;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Date;

@SpringBootTest
public class LogMapperTests {

    @Autowired
    private LogMapper logMapper;

    @Test
    void testInsertLog(){
        Log log=new Log();
        log.setIp("192.168.1.111");
        log.setUsername("Jack");
        log.setOperation("查询资源");
        log.setMethod("pkg.ClassName.methodName");
        log.setParams("10");
        log.setTime(100L);
        log.setCreatedTime(new Date());
        log.setStatus(1);
        logMapper.insert(log);
    }

}
