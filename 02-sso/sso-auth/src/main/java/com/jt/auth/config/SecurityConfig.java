package com.jt.auth.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@Configuration
public class SecurityConfig
        extends WebSecurityConfigurerAdapter {
    /**
     * 使用自定义的账号和密码登录时,需要指定密码的加密算法.
     * 当前这个对象使用的Bcrypt加密算法是一个不可逆的加密算法.
     * 类似MD5,但要比MD5更安全.
     * @return
     */
    @Bean
    public BCryptPasswordEncoder passwordEncoder(){
        return new BCryptPasswordEncoder();
    }

    /**
     * 这个对象暴露给外界,后续我们写OAuth2的配置时会用
     * @return
     * @throws Exception
     */
    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }
}
