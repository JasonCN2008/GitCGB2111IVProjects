package com.jt.auth.service;
import com.jt.auth.pojo.User;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import java.util.List;
/**
 * 远程服务调用接口
 * 1)value属性的值为你要调用的服务名.
 * 2)contextId为当前feign接口的bean名称
 */
@FeignClient(value = "sso-system",
        contextId = "remoteUserService")
public interface RemoteUserService {//明修栈道暗度陈仓

    @GetMapping("/user/login/{username}")
    User selectUserByUsername(
            @PathVariable("username") String username);

    @GetMapping("/user/permission/{userId}")
    List<String> selectUserPermissions(
            @PathVariable("userId") Long userId);
}
