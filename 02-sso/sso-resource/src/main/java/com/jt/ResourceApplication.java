package com.jt;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.scheduling.annotation.EnableAsync;

/**
 * 在当前项目中将资源分为3类:
 * 1)无须认证(登录)就可以访问的资源 (匿名资源)
 * 2)必须认证后才可以访问的资源 (类似我的订单)
 * 3)认证后必须有某些权限才可以访问的资源. (类似访问别人的订单)
 */
@EnableFeignClients
@SpringBootApplication
public class ResourceApplication {
    public static void main(String[] args) {
        SpringApplication.run(ResourceApplication.class,args);
    }
}
