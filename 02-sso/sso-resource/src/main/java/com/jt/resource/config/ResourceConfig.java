package com.jt.resource.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;

/**
 * 定义资源服务器的配置,在这里我们要配置:
 *  1)无须认证(登录)就可以访问的资源 (匿名资源)
 *  2)必须认证后才可以访问的资源 (类似我的订单)
 *  3)认证后必须有某些权限才可以访问的资源. (类似访问别人的订单)
 *
 * 其中:
 * 1)@EnableResourceServer表示启动资源服务配置
 * 2)@EnableGlobalMethodSecurity表示启动方法层面的权限控制(
 * 思考访问资源是否是通过方法,当不允许对这个方法进行访问,就意味
 * 不允许访问资源),prePostEnabled表示在执行资源方法之前要检查是否
 * 有执行此方法的权限.
 */
@EnableGlobalMethodSecurity(prePostEnabled = true)
@EnableResourceServer
@Configuration
public class ResourceConfig
        extends ResourceServerConfigurerAdapter {

    @Override
    public void configure(HttpSecurity http) throws Exception {
        //super.configure(http);
        //1.禁用跨域攻击(假如不禁用,我们使用postman类似的工具发请求时,可能会出现403异常)
        http.csrf().disable();
        //2.配置资源访问规则
        http.authorizeRequests()
                //以"/resource/**"开头的所有url必须认证
                .antMatchers("/resource/**")
                .authenticated()//认证
                //除了上面需要认证的,其它则可以匿名访问
                .anyRequest()
                .permitAll();//表示允许直接访问(匿名访问)
    }
}
