package com.jt.resource.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;

@Configuration
public class TokenConfig {
    /**
     * 定义用户状态的存储方式
     * 1)JdbcTokenStore
     * 2)RedisTokenStore
     * 3)JwtTokenStore
     * 4)InMemoryTokenStore
     * 5)....
     * @return
     */
    @Bean
    public TokenStore tokenStore(){
        return new JwtTokenStore(jwtAccessTokenConverter());
    }

    /**
     * 定义令牌转换器对象,底层会基于此对象
     * 1)将json数据构建为Jwt令牌
     * 2)将Jwt令牌解析为Json字符串
     * @return
     */
    @Bean
    public JwtAccessTokenConverter jwtAccessTokenConverter(){
        //构建JWT令牌转换器,负责将json转换为JWT令牌
        //也可以将jwt令牌转换为json
        JwtAccessTokenConverter converter=
                new JwtAccessTokenConverter();
        //设置JWT令牌的签名key
        converter.setSigningKey(signingKey);
        return converter;
    }
    private String signingKey="auth";
}
