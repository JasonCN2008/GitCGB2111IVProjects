package com.jt.resource.service;

import com.jt.resource.pojo.Log;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * 日志远程服务调用接口，在这里我们要调用system服务去将用户行为日志
 * 写入到数据库中
 */
@FeignClient(value="sso-system",contextId = "remoteLogService")
public interface RemoteLogService {
    @PostMapping("/log")
    void insertLog(@RequestBody Log userLog);
}
