package com.jt.resource.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 希望这个controller中的资源,
 * 都允许匿名访问(不登录就可以访问)
 */
@RestController
@RequestMapping("/other")
public class OtherController {

    @GetMapping
    public String doSelect(){
        return "select other resource";
    }
}
