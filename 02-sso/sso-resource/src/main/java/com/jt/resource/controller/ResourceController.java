package com.jt.resource.controller;

import com.jt.resource.annotation.RequiredLog;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RequestMapping("/resource")
@RestController
public class ResourceController {
    /**
     * @PreAuthorize 注解描述的方法为一个权限切入点方法,
     * 这个方法执行之前首先要进行鉴权,就是判定用户是否有
     * 执行这个方法的权限.,这里的"sys:res:list"表示
     * 访问此方法需要的权限.
     * @return
     */
    @RequiredLog(value="查询资源")
    @GetMapping
    @PreAuthorize("hasAuthority('sys:res:list')")
    public String doSelect(){
        //....
        return "select resource";
    }

    @RequiredLog(value="创建资源")
    @PostMapping
    @PreAuthorize("hasAuthority('sys:res:create')")
    public String doCreate(){
        return "create resource";
    }

    @RequiredLog(value="修改资源")
    @PutMapping
    @PreAuthorize("hasAuthority('sys:res:update')")
    public String doUpdate(){
        return "update resource";
    }

    @DeleteMapping
    @PreAuthorize("hasAuthority('sys:res:delete')")
    public String doDelete(){
        return "delete resource";
    }
}
